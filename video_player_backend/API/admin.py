from django.contrib import admin
from video_player_backend.API.models import History, Bookmark

# Register your models here.
admin.site.register(History)
admin.site.register(Bookmark)