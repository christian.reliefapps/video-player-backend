from django.db import models

# Create your models here.
class History(models.Model):
    video_key = models.CharField(max_length=11, primary_key=True)
    added = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['added']

class Bookmark(models.Model):
    video_key = models.CharField(max_length=11, primary_key=True)
    added = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['added']
