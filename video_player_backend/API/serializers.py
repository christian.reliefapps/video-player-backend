from rest_framework import serializers
from video_player_backend.API.models import History, Bookmark

class HistorySerializer(serializers.ModelSerializer):
   class Meta:
       model = History
       fields = ['video_key', 'added']
    
class BookmarkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bookmark
        fields = ['video_key', 'added']
