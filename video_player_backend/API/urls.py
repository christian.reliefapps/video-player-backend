"""video_player_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from rest_framework import routers
from video_player_backend.API import views
from rest_framework.urlpatterns import format_suffix_patterns
from django.urls import include, path

router = routers.DefaultRouter()
router.register(r'history', views.History_objects)
router.register(r'bookmark', views.Bookmark_objects)

urlpatterns = [
    path('', include(router.urls)),
   path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

"""

urlpatterns = [
    path('history/', views.History_objects.as_view()),
    path('history/<path:entry>/', views.History_objects_details.as_view()),
    path('bookmark/', views.Bookmark_objects.as_view()),
    path('bookmark/<path:entry>/', views.Bookmark_objects_details.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)
"""