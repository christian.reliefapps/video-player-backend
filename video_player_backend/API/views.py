from rest_framework import viewsets, generics
from rest_framework import permissions
from rest_framework import status
from rest_framework.response import Response
from video_player_backend.API.serializers import HistorySerializer, BookmarkSerializer
from video_player_backend.API.models import History, Bookmark

# Create your views here.

class History_objects(viewsets.ModelViewSet):
    queryset = History.objects.all().order_by('added')
    serializer_class = HistorySerializer

    def update(self, request, *args, **kwargs):
        try:
            instance = History.objects.get(pk=kwargs['pk'])
            serializer = HistorySerializer(instance=instance, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except History.DoesNotExist:
            serializer = HistorySerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
"""
class History_objects_details(generics.RetrieveUpdateDestroyAPIView):#viewsets.ModelViewSet):
    queryset = History.objects.all().order_by('added')
    serializer_class = HistorySerializer
"""
class Bookmark_objects(viewsets.ModelViewSet):
    queryset = Bookmark.objects.all().order_by('added')
    serializer_class = BookmarkSerializer

    def update(self, request, *args, **kwargs):
        try:
            instance = Bookmark.objects.get(pk=kwargs['pk'])
            serializer = BookmarkSerializer(instance=instance, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Bookmark.DoesNotExist:
            serializer = BookmarkSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    

"""
class Bookmark_objects_details(generics.RetrieveUpdateDestroyAPIView):#viewsets.ModelViewSet):
    queryset = Bookmark.objects.all().order_by('added')
    serializer_class = BookmarkSerializer
"""